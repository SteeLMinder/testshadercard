Shader "Custom/ShaderCard"
{
    Properties
    {
        [PerRendererData]_MainTex ("MainTex", 2D) = "white" {}
    }
    Category
    {
        Tags
        { 
            "IgnoreProjector"="true"
            "RenderType"="Transparent-1"
            "PreviewType"="Plane"
            "Queue"="Transparent"
        }

        Cull Off
        Lighting Off
        ZWrite On
        ZTest [unity_GUIZTestMode] 
        Blend SrcAlpha OneMinusSrcAlpha
        AlphaTest Greater 0.01

        Fog
        {
            Color (0,0,0,0)
        }
        
        BindChannels
        {
            Bind "Color", color
            Bind "Vertex", vertex
            Bind "TexCoord", texcoord
        }
        
        SubShader
        {
            Pass
            {
                SetTexture [_MainTex]
                {
                    combine texture * primary
                }
            }
        }
    }
}
