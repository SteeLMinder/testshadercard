Shader "Card/GoldenCard_FlowMap"
{
    Properties
    {
        [Header(MainTex)]
        _MainTex ("MainTex", 2D) = "white" {}
        [Header(FlowMap)]
        _FlowMap ("FlowMap", 2D) = "grey" {}
        _FlowTexture ("FlowTexture", 2D) = "white" {}
        _Speed ("Speed", Range(-1, 1)) = 0.2
        _AngleFlow ("AngleFlow", Range(0, 360)) = 0
        _AngleFlowTex ("AngleFlowTex", Range(0, 360)) = 0
        [Header(TexAlpha)]
        _Tex_aplha ("Texture Alpha", 2D) = "white" {}
        _Mask_alpha ("Mask Alpha", 2D) = "white" {}
        _U_pic_a ("Speed Alpha X", float ) = -1
        _V_pic_a ("Speed Alpha Y", float ) = -0.1
        _Rotate ("Angle Alpha °С", Range(0, 360)) = 0
        _RotateMaskAlpha ("Angle Mask Alpha °С", Range(0, 360)) = 0
        [Header(TexAlpha 2)]
        _Tex_aplha_2 ("Texture Alpha", 2D) = "white" {}
        _Mask_alpha_2 ("Mask Alpha", 2D) = "white" {}
        _U_pic_a_2 ("Speed Alpha X", float ) = -1
        _V_pic_a_2 ("Speed Alpha Y", float ) = -0.1
        _Rotate_2 ("Angle Alpha °С", Range(0, 360)) = 0
        _RotateMaskAlpha_2 ("Angle Mask Alpha °С", Range(0, 360)) = 0
        [Header(Glow)]
        _MaskGlow ("MaskGlow", 2D) = "white" {}
        _GlowTex ("GlowTex", 2D) = "white" {}
        _SpeedX ("Speed Glow X", float) = 0
        _SpeedY ("Speed Glow Y", float) = 0
        _Angle ("Angle Glow °С", Range(0, 360)) = 0
        [Header(Glow)]
        _MaskGlow_2 ("MaskGlow", 2D) = "white" {}
        _GlowTex_2 ("GlowTex", 2D) = "white" {}
        _SpeedX_2 ("Speed Glow X", float) = 0
        _SpeedY_2 ("Speed Glow Y", float) = 0
        _Angle_2 ("Angle Glow °С", Range(0, 360)) = 0
        [Header(Glow)]
        _MaskGlow_3 ("MaskGlow", 2D) = "white" {}
        _GlowTex_3 ("GlowTex", 2D) = "white" {}
        _SpeedX_3 ("Speed Glow X", float) = 0
        _SpeedY_3 ("Speed Glow Y", float) = 0
        _AngleMask_3 ("Angle Mask °С", Range(0, 360)) = 0
        _Angle_3 ("Angle Glow °С", Range(0, 360)) = 0
    }

    CGINCLUDE
    #include "UnityCG.cginc"

    half2 PointRotate(half angle, half2 uv)
    {
        const half2 pointRotate = half2(0.5, 0.5);
        const half Cosin = cos(1.0 * (angle * 0.0175));
        const half Sinus = sin(1.0 * (angle * 0.0175));
        const half2 rotate = (mul(uv - pointRotate, half2x2( Cosin, -Sinus, Sinus, Cosin)) + pointRotate);
        return rotate;
    }
    //MainTex
    uniform sampler2D _MainTex;
    //FlowMap                       //GlowTex_3
    uniform sampler2D _FlowTexture; uniform sampler2D _GlowTex_3;
    uniform half4 _FlowTexture_ST;  uniform half4 _GlowTex_3_ST;
    uniform sampler2D _FlowMap;     uniform sampler2D _MaskGlow_3;
    uniform half4 _FlowMap_ST;      uniform half4 _MaskGlow_3_ST;
    uniform half _Speed;            uniform half _SpeedX_3;
    uniform half _AngleFlow;        uniform half _SpeedY_3;
    uniform half _AngleFlowTex;     uniform half _Angle_3;
                                    uniform half _AngleMask_3;
    //TexAlpha                      //TexAlpha
    uniform sampler2D _Tex_aplha;   uniform sampler2D _Tex_aplha_2;
    uniform half4 _Tex_aplha_ST;    uniform half4 _Tex_aplha_2_ST;
    uniform sampler2D _Mask_alpha;  uniform sampler2D _Mask_alpha_2;
    uniform half4 _Mask_alpha_ST;   uniform half4 _Mask_alpha_2_ST;
    uniform half _U_pic_a;          uniform half _V_pic_a_2;
    uniform half _V_pic_a;          uniform half _U_pic_a_2;
    uniform half _Rotate;           uniform half _Rotate_2;
    uniform half _RotateMaskAlpha;  uniform half _RotateMaskAlpha_2;
    //GlowTex                       //GlowTex_2
    uniform sampler2D _MaskGlow;    uniform sampler2D _GlowTex_2;
    uniform half4 _MaskGlow_ST;     uniform half4 _GlowTex_2_ST;
    uniform sampler2D _GlowTex;     uniform sampler2D _MaskGlow_2;
    uniform half4 _GlowTex_ST;      uniform half4 _MaskGlow_2_ST;
    uniform half _SpeedX;           uniform half _SpeedX_2;
    uniform half _SpeedY;           uniform half _SpeedY_2;
    uniform half _Angle;            uniform half _Angle_2;

    ENDCG

    SubShader
    {
        
        Tags
        {
            "IgnoreProjector"="true"
            "Queue"="Transparent"
            "RederType"="Transparent-1"
            "PreviewType"="Plane"
        }

        Cull Off
        ZWrite On
        ZTest [unity_GUIZTestMode]   
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma fragment fragMain
            #pragma vertex vertMain 
            #pragma debug
            #pragma fragmentoption ARB_precision_hint_fastest

            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv     : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 pos    : SV_POSITION;
                float2 uv0    : TEXCOORD0;
                float2 uv1    : TEXCOORD1;
                float2 uv2    : TEXCOORD2;
                float2 uv3    : TEXCOORD3;
                float2 uv4    : TEXCOORD4;
            };

            VertexOutput vertMain (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;
                //MainTex UV
                o.uv0.xy = v.uv.xy;  
                //Rotatate Alpha
                o.uv1 = TRANSFORM_TEX(PointRotate(_Rotate, v.uv.xy), _Tex_aplha);
                o.uv1.x -= _U_pic_a * _Time;
                o.uv1.y -= _V_pic_a * _Time;
                //RotateAlpha_2
                o.uv2 = TRANSFORM_TEX(PointRotate(_Rotate_2, v.uv.xy), _Tex_aplha_2);
                o.uv2.x -= _U_pic_a_2 * _Time;
                o.uv2.y -= _V_pic_a_2 * _Time;
                //RotateMaskAlpha
                o.uv3.xy = TRANSFORM_TEX(PointRotate(_RotateMaskAlpha, v.uv.xy), _Mask_alpha);
                //RotateMaskAlpha_2
                o.uv4.xy = TRANSFORM_TEX(PointRotate(_RotateMaskAlpha_2, v.uv.xy), _Mask_alpha_2);               
                //Unity Matrix
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }

            half4 fragMain(VertexOutput i) : SV_TARGET
            {
                //MainTex
                half4 MainTex = tex2D(_MainTex, i.uv0);
                //Alpha Textures
                half4 Alpha = tex2D(_Tex_aplha, i.uv1);
                half4 Alpha_2 = tex2D(_Tex_aplha_2, i.uv2);
                //Mask Textures
                half4 MaskAlpha = tex2D(_Mask_alpha, i.uv3);
                half4 MaskAlpha_2 = tex2D(_Mask_alpha_2, i.uv4);
                //FinColor            
                half3 FinalColor = lerp(Alpha.rgb, MainTex.rgb, lerp(MainTex.a,(1.0 - Alpha.a), MaskAlpha.r));
                half3 FinColorWithAlpha_2 = lerp(Alpha_2.rgb, FinalColor, lerp(MainTex.a,(1.0 - Alpha_2.a), MaskAlpha_2.r));   

                return fixed4(FinColorWithAlpha_2, 1);
            } 
            ENDCG
        }

        Pass
        {

            CGPROGRAM
            #pragma fragment fragFlow
            #pragma vertex vertMain
            #pragma debug
            #pragma fragmentoption ARB_precision_hint_fastest  

            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv     : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 pos    : SV_POSITION;
                float2 uv0    : TEXCOORD0;
                float2 uv1    : TEXCOORD1;
            };

            VertexOutput vertMain (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;
                //FlowTexture
                o.uv0 = TRANSFORM_TEX(PointRotate(_AngleFlowTex, v.uv.xy), _FlowTexture);  
                o.uv1 = TRANSFORM_TEX(PointRotate(_AngleFlow, v.uv.xy), _FlowMap);
                //Unity Matrix
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

                return o;
            }

            half4 fragFlow(VertexOutput i) : SV_TARGET
            {
                half4 FlowMap = (tex2D(_FlowMap, i.uv0) * 2.0 - 1.0) * _Speed;

                half dif1 = frac(_Time.y * 0.25 + 0.5);
                half dif2 = frac(_Time.y * 0.25 + 1.0);

                half flowLerp = abs((0.5 - dif1)/0.5);

                half4 text1 = tex2D(_FlowTexture, i.uv0 - FlowMap.xy * dif1);
                half4 text2 = tex2D(_FlowTexture, i.uv0 - FlowMap.xy * dif2);

                fixed4 textLerp = lerp(text1, text2, flowLerp);

                return textLerp;
            }   
            ENDCG
        }

        Pass
        {

            Blend One One
            CGPROGRAM
            #pragma fragment fragGlow
            #pragma vertex vertMain
            #pragma debug
            #pragma fragmentoption ARB_precision_hint_fastest

            struct VertexInput
            {
                float4 vertex : POSITION;
                float2 uv     : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 pos    : SV_POSITION;
                float2 uv0    : TEXCOORD0;
                float2 uv1    : TEXCOORD1;
                float2 uv2    : TEXCOORD2;
                float2 uv3    : TEXCOORD3;
                float2 uv4    : TEXCOORD4;
            };

            VertexOutput vertMain (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;
                //MainTex UV
                o.uv0 = v.uv.xy;  
                //Transfer UVGlow to Vertex Shader
                o.uv1 = TRANSFORM_TEX(PointRotate(_Angle, v.uv.xy), _GlowTex);
                o.uv1.x -= _SpeedX * _Time;
                o.uv1.y -= _SpeedY * _Time;
                //Transfer UVGlow to Vertex Shader
                o.uv2 = TRANSFORM_TEX(PointRotate(_Angle_2, v.uv.xy), _GlowTex_2);
                o.uv2.x -= _SpeedX_2 * _Time;
                o.uv2.y -= _SpeedY_2 * _Time;
                //GlowTex_3
                o.uv3 = TRANSFORM_TEX(PointRotate(_Angle_3, v.uv.xy), _GlowTex_3);
                o.uv3.x -= _SpeedX_3 * _Time;
                o.uv3.y -= _SpeedY_3 * _Time;
                //MaskGlow_3
                o.uv4 = TRANSFORM_TEX(PointRotate(_AngleMask_3, v.uv.xy), _MaskGlow_3);
                //Unity Matrix
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);

                return o;
            }

            half4 fragGlow(VertexOutput i) : SV_TARGET
            {
                //Texture Glow 2
                half4 MaskGlow_2 = tex2D(_MaskGlow, TRANSFORM_TEX(i.uv0, _MaskGlow));
                half4 MaskGlow_3 = tex2D(_MaskGlow_2, TRANSFORM_TEX(i.uv0, _MaskGlow_2));
                half4 GlowTex = tex2D(_GlowTex, i.uv1);
                half4 GlowTex_2 = tex2D(_GlowTex_2, i.uv2);
                //Add GlowTestures
                half3 GlowTestures = (MaskGlow_3.rgb * GlowTex_2.rgb) + (MaskGlow_2.rgb * GlowTex.rgb);
                half4 MaskGlow = tex2D(_MaskGlow_3, i.uv4);
                half4 GlowTex3 = tex2D(_GlowTex_3, i.uv3);
                half3 FinGlow = (GlowTex3.rgb * MaskGlow.rgb) + GlowTestures;

                return fixed4(FinGlow, 1);
            }
            ENDCG
        }
    }
}
 