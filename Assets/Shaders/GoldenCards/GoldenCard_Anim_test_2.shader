Shader "Card/GoldenCard_test_2"
{
    Properties
    {
        [Header(MainTex)]
        _MainTex ("MainTex", 2D) = "white" {}
        [Header(Noise 2)]
        _Noise_2 ("Noise 2", 2D) = "white" {}
        _SpeedNoise1R_X ("Speed Noise 2 X", float ) = 1
        _SpeedNoise1R_Y ("Speed Noise 2 Y", float ) = 0
        _PowerNoise2 ("PowerNoise2", Range(-1, 1)) = 0
        [Header(Distortion)]
        _mask_distortUV ("Mask Distort UV", 2D) = "white" {}
        _mask_Glow ("Mask Glow", 2D) = "white" {}
        _color_Glow ("Color Glow", Color) = (1,1,0.1,1)
        _power_Glow ("Power Glow", float ) = 1
        [Header(Noise 1)]
        [MaterialToggle] _addNoise1 ("Add Noise 1", float ) = 0
        _SpeedNoise_X ("Speed Noise 1 X", float) = 0
        _SpeedNoise_Y ("Speed Noise 1 Y", float) = 0
        [Header(TexAlpha)]
        _Tex_aplha ("Texture Alpha", 2D) = "white" {}
        _Tex_aplha_Tint ("Texture Alpha Tint", Color) = (0,0,0.0,1)
        _Mask_alpha ("Mask Alpha", 2D) = "white" {}
        _U_pic_a ("Speed Alpha X", float ) = -1
        _V_pic_a ("Speed Alpha Y", float ) = -0.1
        _Rotate ("Angle Alpha °С", Range(0, 360)) = 0
        _RotateMaskAlpha ("Angle Mask Alpha °С", Range(0, 360)) = 0
        [Header(TexAlpha 2)]
        _Tex_aplha_2 ("Texture Alpha", 2D) = "white" {}
        _Tex_aplha_Tint_2 ("Texture Alpha Tint", Color) = (0,0,0.0,1)
        _Mask_alpha_2 ("Mask Alpha", 2D) = "white" {}
        _U_pic_a_2 ("Speed Alpha X", float ) = -1
        _V_pic_a_2 ("Speed Alpha Y", float ) = -0.1
        _Rotate_2 ("Angle Alpha °С", Range(0, 360)) = 0
        _RotateMaskAlpha_2 ("Angle Mask Alpha °С", Range(0, 360)) = 0
        [Header(Glow)]
        _MaskGlow ("MaskGlow", 2D) = "white" {}
        _GlowTex ("GlowTex", 2D) = "white" {}
        _GlowTex_Tint ("GlowTex Tint", Color) = (0,0,0.0,1)
        _SpeedX ("Speed Glow X", float) = 0
        _SpeedY ("Speed Glow Y", float) = 0
        _Angle_Mask ("Angle Mask °С", Range(0, 360)) = 0
        _Angle ("Angle Glow °С", Range(0, 360)) = 0
        [Header(Glow 2)]
        _MaskGlow_3 ("MaskGlow", 2D) = "white" {}
        _GlowTex_3 ("GlowTex", 2D) = "white" {}
        _GlowTex_Tint_3 ("GlowTex Tint", Color) = (0,0,0.0,1)
        _SpeedX_3 ("Speed Glow X", float) = 0
        _SpeedY_3 ("Speed Glow Y", float) = 0
        _Angle_Mask_3 ("Angle Mask °С", Range(0, 360)) = 0
        _Angle_3 ("Angle Glow °С", Range(0, 360)) = 0
    }

    CGINCLUDE
    #include "UnityCG.cginc"

    half2 PointRotate(half angle, half2 uv)
    {
        const half2 pointRotate = half2(0.5, 0.5);
        half Cosin = cos(1.0 * (angle * 0.0175));
        half Sinus = sin(1.0 * (angle * 0.0175));
        half2 rotate = mul(uv - pointRotate, half2x2( Cosin, -Sinus, Sinus, Cosin)) + pointRotate;
        return rotate;
    }
    //testures sampler2D                //Textures _ST
    uniform sampler2D _MainTex;         uniform half4 _MainTex_ST;
    uniform sampler2D _mask_distortUV;  uniform half4 _mask_distortUV_ST;
    uniform sampler2D _mask_Glow;       uniform half4 _mask_Glow_ST;
    uniform sampler2D _Tex_aplha;       uniform half4 _Tex_aplha_ST;
    uniform sampler2D _Mask_alpha;      uniform half4 _Mask_alpha_ST;
    uniform sampler2D _Noise_2;         uniform half4 _Noise_2_ST;
    uniform sampler2D _MaskGlow;        uniform half4 _MaskGlow_ST;
    uniform sampler2D _MaskGlow_3;      uniform half4 _MaskGlow_3_ST;
    uniform sampler2D _GlowTex;         uniform half4 _GlowTex_ST;
    uniform sampler2D _GlowTex_3;       uniform half4 _GlowTex_3_ST;
    uniform sampler2D _Tex_aplha_2;     uniform half4 _Tex_aplha_2_ST;
    uniform sampler2D _Mask_alpha_2;    uniform half4 _Mask_alpha_2_ST;
    //Speeds                            //rotate
    uniform half _SpeedNoise1R_X;       uniform half _RotateMaskAlpha;
    uniform half _SpeedNoise1R_Y;       uniform half _Rotate;
    uniform half _SpeedNoise_X;         uniform half _Angle_Mask;
    uniform half _SpeedNoise_Y;         uniform half _Angle;
    uniform half _SpeedX;               uniform half _Angle_3;
    uniform half _SpeedY;               uniform half _Angle_Mask_3;
    uniform half _SpeedX_3;             uniform half _Rotate_2;
    uniform half _SpeedY_3;             uniform half _RotateMaskAlpha_2;
    uniform half _V_pic_a_2;            
    uniform half _U_pic_a_2;
    uniform half _V_pic_a;
    uniform half _U_pic_a;
    //textures Tint
    uniform half4 _Tex_aplha_Tint;
    uniform half4 _GlowTex_Tint;
    uniform half4 _GlowTex_Tint_3;
    uniform half4 _Tex_aplha_Tint_2;

    uniform half _PowerNoise2;  
    uniform half4 _color_Glow;
    uniform half _power_Glow;
    uniform fixed _addNoise1;
    ENDCG

    SubShader
    {

        Tags
        {
            "IgnoreProjector"="true"
            "Queue"="Transparent"
            "RederType"="Transparent-1"
            "PreviewType"="Plane"
        }

        Blend SrcAlpha OneMinusSrcAlpha
        Cull Off
        ZWrite On
        ZTest [unity_GUIZTestMode]

        Pass
        {  
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag 
            #pragma debug
            #pragma fragmentoption ARB_precision_hint_fastest

            struct VertexInput
            {
                float4 vertex    : POSITION;
                float4 texcoord0 : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 pos       : SV_POSITION;
                float2 uv0       : TEXCOORD0;
                float2 uv1       : TEXCOORD1;
                float2 uv2       : TEXCOORD2;
                float2 uv3       : TEXCOORD3;
                float2 uv4       : TEXCOORD4;
                float2 uv5       : TEXCOORD5;
                float2 uv6       : TEXCOORD6;
            };

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;
                //MainTex UV
                o.uv0 = v.texcoord0;  
                //UV Noise_1
                o.uv1 = TRANSFORM_TEX(v.texcoord0, _Noise_2);
                o.uv1.x -= _SpeedNoise_X * _Time;
                o.uv1.y -= _SpeedNoise_Y * _Time;
                //UV Noise_2
                o.uv2 = TRANSFORM_TEX(v.texcoord0, _Noise_2);
                o.uv2.x -= _SpeedNoise1R_X * _Time;
                o.uv2.y -= _SpeedNoise1R_Y * _Time;
                //Rotatate Alpha
                o.uv3 = TRANSFORM_TEX(PointRotate(_Rotate, o.uv0),_Tex_aplha);
                o.uv3.x -= _U_pic_a * _Time;
                o.uv3.y -= _V_pic_a * _Time;
                //RotateAlpha_2
                o.uv4 = TRANSFORM_TEX(PointRotate(_Rotate_2, o.uv0),_Tex_aplha_2);
                o.uv4.x -= _U_pic_a_2 * _Time;
                o.uv4.y -= _V_pic_a_2 * _Time;
                //RotateMaskAlpha
                o.uv5 = TRANSFORM_TEX(PointRotate(_RotateMaskAlpha, o.uv0), _Mask_alpha);
                //RotateMaskAlpha_2
                o.uv6 = TRANSFORM_TEX(PointRotate(_RotateMaskAlpha_2, o.uv0), _Mask_alpha_2);
                //Unity Matrix
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }

            half4 frag(VertexOutput i) : SV_TARGET
            {
                //Alpha Textures
                float4 Alpha = tex2D(_Tex_aplha, i.uv3)  * _Tex_aplha_Tint * _Tex_aplha_Tint.a;
                float4 Alpha_2 = tex2D(_Tex_aplha_2, i.uv4) * _Tex_aplha_Tint_2 * _Tex_aplha_Tint_2.a;
                //Noise Texture 2
                float4 Noise = tex2D(_Noise_2, i.uv1);
                float4 Noise2 = tex2D(_Noise_2, i.uv2);
                //Mask Textures
                float4 MaskAlpha = tex2D(_Mask_alpha, i.uv5);
                float4 MaskAlpha2 = tex2D(_Mask_alpha_2, i.uv6);
                //Distortion Noise
                float4 MaskDistr = tex2D(_mask_distortUV, i.uv0);
                //MainTex
                float2 UVMainTex = lerp(i.uv0, lerp(i.uv0,(i.uv0.rg + Noise2.rg) * 0.5, _PowerNoise2), MaskDistr.rg);
                float4 MainTex = tex2D(_MainTex, UVMainTex);
                //Texture Glow 1
                half4 MaskGlow = tex2D(_mask_Glow, TRANSFORM_TEX(i.uv0, _mask_Glow));
                half3 PowGlow = ((MaskGlow.rgb * _color_Glow.rgb) * _power_Glow);
                //FinalColor
                float3 AlphaColor = lerp(Alpha.rgb, MainTex.rgb, lerp(MainTex.a,(1.0 - Alpha.a), MaskAlpha.r));
                float3 FinalColor = lerp(Alpha_2.rgb, (AlphaColor + lerp(PowGlow, (Noise.rgb * PowGlow), _addNoise1 )), lerp(MainTex.a,(1.0 - Alpha_2.a), MaskAlpha2.r));
                       
                return fixed4(FinalColor, 1);
            }
            ENDCG
        }

        Pass
        {   
            Blend SrcAlpha One
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment fragGlow 
            #pragma debug
            #pragma fragmentoption ARB_precision_hint_fastest

            struct VertexInput
            {
                float4 vertex : POSITION;
                float4 uv     : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 pos    : SV_POSITION;
                float2 uv0    : TEXCOORD0;
                float2 uv1    : TEXCOORD1;
                float2 uv2    : TEXCOORD2;
                float2 uv3    : TEXCOORD3;
            };

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;
                //Transfer UVGlow to Vertex Shader
                o.uv0 = TRANSFORM_TEX(PointRotate(_Angle, v.uv.xy), _GlowTex);
                o.uv0.x -= _SpeedX * _Time;
                o.uv0.y -= _SpeedY * _Time;
                //RotateGlow
                o.uv1 = TRANSFORM_TEX(PointRotate(_Angle_3, v.uv.xy), _GlowTex_3);
                o.uv1.x -= _SpeedX_3 * _Time;
                o.uv1.y -= _SpeedY_3 * _Time;
                //MaskGlow
                o.uv2 = TRANSFORM_TEX(PointRotate(_Angle_Mask, v.uv.xy), _MaskGlow);
                //MaskGlow
                o.uv3 = TRANSFORM_TEX(PointRotate(_Angle_Mask_3, v.uv.xy), _MaskGlow_3);   
                //Unity Matrix
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                return o;
            }

            half4 fragGlow(VertexOutput i) : SV_TARGET
            {
                float4 MaskGlow2 = tex2D(_MaskGlow, i.uv2);
                float4 GlowTex = tex2D(_GlowTex, i.uv0) * _GlowTex_Tint * _GlowTex_Tint.a;
                //Texture Glow 3
                float4 MaskGlow3 = tex2D(_MaskGlow_3, i.uv3);
                float4 GlowTex3 = tex2D(_GlowTex_3, i.uv1) * _GlowTex_Tint_3 * _GlowTex_Tint_3.a; 
                //FinGlowColor
                float4 FinGlowTex = MaskGlow2 * GlowTex + MaskGlow3 * GlowTex3;
                       
                return fixed4(FinGlowTex.rgb, 1);
            }
            ENDCG
        }
    }
} 
 