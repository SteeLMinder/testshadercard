Shader "Card Effects/ARMR2Fragment"
{
    Properties
    {
        [Header(MainTex)]
        _MainTex ("Main texture (RGB)", 2D) = "white" {}
        [Header(Noise Map)]
        _NoiseMap ("Noise", 2D) = "white" {}
        _mask_distortUV ("Mask Distort UV", 2D) = "white" {}
        _SpeedNoise_X ("Speed Noise 2 X", Float ) = 1
        _SpeedNoise_Y ("Speed Noise 2 Y", Float ) = 0
        _PowerNoise2 ("PowerNoise2", Range(-1, 1)) = 0
        [Header(Glow)]
        _ColorGlow ("Color Glow", Color) = (1,1,1,1)
        _TintGlow ("Tint Glow", float) = 0
        _GlowTexture ("Glow Texture (RGB)", 2D) = "white" {}
        [Header(Fire)]
        _ColorFire ("Color Fire", Color) = (1,1,1,1)
        _TintFire ("Tint Fire", float) = 0
        _FireTexture ("Fire Texture (RGB)", 2D) = "white" {}
        [Header(Texture 1)]
        _TintTex_1 ("Tint Texture 1", Color) = (1,1,1,1)
        _SpeedTex_1 ("Speed Tex 1", vector) = (0,0,0,0)
        _Tex_1 ("Texture 1 (RGB)", 2D) = "white" {}
        _Mask_1 ("Mask 1 (A)", 2D) = "white" {}
        [Header(Texture 2)]
        _TintTex_2 ("Tint Texture 2", Color) = (1,1,1,1)
        _SpeedTex_2 ("Speed Tex 2", vector ) = (0,0,0,0)
        _Tex_2 ("Texture 2 (RGB)", 2D) = "white" {}
        _Mask_2 ("Mask 2 (A)", 2D) = "white" {}
        [Header(Texture 3)]
        _SpeedTex_3 ("Speed Tex 3", vector ) = (0,0,0,0)
        _Tex_3 ("Texture 3 (RGB)", 2D) = "white" {}
    }

    SubShader
    {
        Tags
        {
            "IgnoreProjector"="true"
            "Queue"="Transparent"
            "RederType"="Transparent-1"
            "PreviewType"="Plane"
        }

        Cull Off
        ZWrite On
        ZTest [unity_GUIZTestMode]   
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma fragmentoption ARB_precision_hint_fastest
            #include "UnityCG.cginc"

            uniform float4 _TimeEditor;
            uniform sampler2D _MainTex;
            uniform float4 _MainTex_ST;
            uniform sampler2D _GlowTexture;
            uniform float4 _GlowTexture_ST;
            uniform sampler2D _FireTexture;
            uniform float4 _FireTexture_ST;
            uniform sampler2D _Tex_1;
            uniform float4 _TintTex_1;
            uniform float4 _Tex_1_ST;
            uniform sampler2D _Mask_1;
            uniform float4 _Mask_1_ST;
            uniform sampler2D _Tex_2;
            uniform float4 _TintTex_2;
            uniform float4 _Tex_2_ST;
            uniform sampler2D _Tex_3;
            uniform float4 _Tex_3_ST;
            uniform sampler2D _Mask_2;
            uniform float4 _Mask_2_ST;
            uniform float4 _SpeedTex_1;
            uniform float4 _SpeedTex_2;
            uniform float4 _SpeedTex_3;
            uniform float4 _ColorGlow;
            uniform float4 _ColorFire;
            uniform float _TintGlow;
            uniform float _TintFire;
            uniform sampler2D _NoiseMap;
            uniform float4 _NoiseMap_ST;
            uniform float4 _SpeedFlow;
            uniform sampler2D _mask_distortUV;
            uniform float4 _mask_distortUV_ST;
            uniform float _SpeedNoise_X;
            uniform float _SpeedNoise_Y;
            uniform float _PowerNoise2;

            struct VertexInput
            {
                float4 vertex  : POSITION;
                float2 uv      : TEXCOORD0;
            };

            struct VertexOutput
            {
                float4 pos    : SV_POSITION;
                float2 uv0    : TEXCOORD0;
                float2 uv1    : TEXCOORD1;
                float2 uv2    : TEXCOORD2;
                float2 uv3    : TEXCOORD3;
                float2 uv4    : TEXCOORD4;
            };

            VertexOutput vert (VertexInput v)
            {
                VertexOutput o = (VertexOutput)0;

                o.uv0 = TRANSFORM_TEX(v.uv, _MainTex); 
                //Texture 1
                o.uv1 = TRANSFORM_TEX(v.uv, _Tex_1);
                o.uv1 -= _SpeedTex_1 * _Time; 
                //Texture 2
                o.uv2 = TRANSFORM_TEX(v.uv, _Tex_2);
                o.uv2 -= _SpeedTex_2 * _Time;
                //Texture 3
                o.uv3 = TRANSFORM_TEX(v.uv, _Tex_3);
                o.uv3 -= _SpeedTex_3 * _Time;
                //NoiseMap
                o.uv4 = TRANSFORM_TEX(v.uv, _NoiseMap);
                o.uv4.x -= _SpeedNoise_X * _Time;
                o.uv4.y -= _SpeedNoise_Y * _Time;
                //Unity Matrix    
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }

            half4 frag(VertexOutput i) : SV_TARGET
            {

                float4 noiseMap = tex2D(_NoiseMap, i.uv4);
                float4 distortUV = tex2D(_mask_distortUV, i.uv0);
                float2 Noise_MainTex = lerp(i.uv0, lerp(i.uv0, (i.uv0.rg + noiseMap.rg) * 0.5, _PowerNoise2), distortUV.rg);
                float4 MainTex = tex2D(_MainTex, Noise_MainTex);
                //GLow and Firw
                float4 GlowTex = tex2D(_GlowTexture, TRANSFORM_TEX(i.uv0, _GlowTexture));
                float4 FireTex = tex2D(_FireTexture, TRANSFORM_TEX(i.uv0, _FireTexture));
                //texture 1
                float4 Tex_1 = tex2D(_Tex_1, i.uv1) * _TintTex_1;
                float4 Mask_1 = tex2D(_Mask_1, i.uv0);
                //texture 2
                float4 Tex_2 = tex2D(_Tex_2, i.uv2) * _TintTex_2;
                float4 Mask_2 = tex2D(_Mask_2, i.uv0);
                //texture 3
                float4 _Tex_3_var = tex2D(_Tex_3, i.uv3);
                //result
                float3 finalColor = (FireTex.rgb * _ColorFire * _TintFire) + (GlowTex.rgb * _ColorGlow * _TintGlow)
                 + MainTex.rgb + Tex_1.rgb * Mask_1.rgb + Tex_2.rgb * Mask_2.rgb + _Tex_3_var.rgb * Mask_2.rgb;

                return fixed4(finalColor, MainTex.a);
            }
            ENDCG
        }
    }
}
